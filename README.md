# Bitbucket webhooks listener

## Install

Depends on python3 and tornado

```
sudo apt-get install python3 python3-pip
sudo pip3 install tornado
```

## Usage:

```
./listener.py --port=PORT
```

## Configuration

actions.json defines commands to be run with the following structure:

```
{
  "REPO_NAME": {
    "path": "REPO_PATH",
    "actions": {
      "BRANCH_NAME": "COMMAND"
    }
  }
}

REPO_NAME: repo full_name (eg. "gpiffault/bitbucket-webhooks") or name (eg. "bitbucket-webhooks"), full_name takes precedence
REPO_PATH: working directory is set to this value before running commands
BRANCH_NAME: command is run when this branch is updated (eg. "master")
COMMAND: command to run (eg. "git pull", "./build", "make"), command is run in a new process to avoid webhooks timeouts
```

Warning: commands are run with the same privileges as the user who started listener.py, use with caution
