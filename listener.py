#!/usr/bin/env python3
import logging
import socket
import json
import os
import subprocess

import tornado.ioloop
import tornado.web
from tornado.options import define, options


logger = logging.getLogger("main")

define("port", default=9050, help="Listen on port")


class MainHandler(tornado.web.RequestHandler):

    def get(self):
        self.write("bitbucket-webhooks")

    def post(self):
        actions = json.load(open("actions.json", encoding="utf-8"))
        data = json.loads(self.request.body.decode("utf-8"))

        repo = None
        if data["repository"]["full_name"] in actions:
            repo = actions[data["repository"]["fullname"]]
        elif data["repository"]["name"] in actions:
            repo = actions[data["repository"]["name"]]

        if repo is None:
            return

        prev_dir = os.getcwd()
        os.chdir(repo["path"])

        for change in data["push"]["changes"]:
            if change["new"] is None:
                continue
            if change["new"]["name"] not in repo["actions"]:
                continue
            subprocess.Popen(repo["actions"][change["new"]["name"]], shell=True)

        os.chdir(prev_dir)


application = tornado.web.Application([
    (r"/", MainHandler),
])


if __name__ == "__main__":
    tornado.options.parse_command_line()

    application.listen(options.port)

    ioloop = tornado.ioloop.IOLoop.current()
    ioloop.add_callback(logger.info, "server now running on {}:{}".format(
        socket.gethostname(), options.port))
    ioloop.start()
